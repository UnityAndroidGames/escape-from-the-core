﻿using System.Collections;
using UnityEngine.UI;
using UnityEngine;
using GooglePlayGames;


public class ButtonsController : MonoBehaviour
{
    [SerializeField] private Sprite[] buttonSprites;
    [SerializeField] private GameObject down, prefabShuttle;
    private GameObject shuttle;
    public void Start()
    {
        //Установка иконок кнопок настроек
        if (PlayerPrefs.GetString("Sound") == "on")
            transform.GetChild(5).gameObject.transform.GetChild(0).GetComponent<Image>().sprite = buttonSprites[0];
        else
            transform.GetChild(5).gameObject.transform.GetChild(0).GetComponent<Image>().sprite = buttonSprites[1];

        if (PlayerPrefs.GetString("Music") == "on")
            transform.GetChild(5).gameObject.transform.GetChild(1).GetComponent<Image>().sprite = buttonSprites[2];
        else
            transform.GetChild(5).gameObject.transform.GetChild(1).GetComponent<Image>().sprite = buttonSprites[3];
    }

//Настройки
    public void Settings()
    {
        transform.GetChild(5).gameObject.SetActive(true);
        transform.GetChild(3).gameObject.SetActive(false);
    }

    public void MusicOnOff()
    {
        if (transform.GetChild(5).gameObject.transform.GetChild(1).GetComponent<Image>().sprite == buttonSprites[2])
        {
            PlayerPrefs.SetString("Music", "off");
            transform.GetChild(5).gameObject.transform.GetChild(1).GetComponent<Image>().sprite = buttonSprites[3];
        }
        else
        {
            PlayerPrefs.SetString("Music", "on");
            transform.GetChild(5).gameObject.transform.GetChild(1).GetComponent<Image>().sprite = buttonSprites[2];
        }
    }
    public void SoundOnOff()
    {
        if (transform.GetChild(5).gameObject.transform.GetChild(0).GetComponent<Image>().sprite == buttonSprites[0])
        {
            PlayerPrefs.SetString("Sound", "off");
            transform.GetChild(5).gameObject.transform.GetChild(0).GetComponent<Image>().sprite = buttonSprites[1];
        }
        else
        {
            PlayerPrefs.SetString("Sound", "on");
            transform.GetChild(5).gameObject.transform.GetChild(0).GetComponent<Image>().sprite = buttonSprites[0];
        }
    }
    public void CloseSettings()
    {
        transform.GetChild(3).gameObject.SetActive(true);
        transform.GetChild(5).gameObject.SetActive(false);
    }


//Игра
    public void Play()
    {
        transform.GetChild(4).GetComponent<Text>().text = PlayerPrefs.GetInt("Record").ToString();
        transform.GetChild(3).gameObject.SetActive(false);
        shuttle = Instantiate(prefabShuttle);
        down.GetComponent<GameController>().stopGame = false;
        down.GetComponent<GameController>().shuttle = GameObject.FindGameObjectWithTag("Player");
        down.GetComponent<BoxCollider2D>().enabled = true;
        Time.timeScale = 1;
        StartCoroutine(DownEnable_WaitForSeconds(1f));
        GameObject.Find("RoadGenerator").GetComponent<RoadGenerator>().enabled = true;
        if (PlayerPrefs.GetString("Sound") == "on")
        {
            shuttle.GetComponent<AudioSource>().enabled = true;
            down.GetComponent<AudioSource>().enabled = true;
            shuttle.transform.GetChild(4).GetComponent<AudioSource>().enabled = true;
        }
        if (PlayerPrefs.GetString("Music") == "on")
        {
            GetComponent<AudioSource>().enabled = true;
        }
    }
    private IEnumerator DownEnable_WaitForSeconds(float value) //Зажечь "огонь" внизу экрана
    {
        yield return new WaitForSeconds(value);
        down.GetComponent<SpriteRenderer>().enabled = true;
        down.GetComponent<BoxCollider2D>().enabled = true;
        shuttle.GetComponent<Animator>().enabled = false;
    }
    public void Exit()
    {
        PlayerPrefs.SetInt("AdCount", 0);
        PlayGamesPlatform.Instance.SignOut();
        Application.Quit();
    }

    public void Rate()
    {
        Application.OpenURL("market://details?id=com.SWG.Escapefromthecore");
    }

    public void ShowLeaderBoard()
    {
        Social.ShowLeaderboardUI();
    }


//Управление шаттлом
    public void LeftButtonDown()
    {
        shuttle.GetComponent<MoveController>().LeftButtonDown();
    }

    public void RightButtonDown()
    {
        shuttle.GetComponent<MoveController>().RightButtonDown();
    }

    public void LeftRightButtonUp()
    {
        shuttle.GetComponent<MoveController>().LeftRightButtonUp();
    }
}