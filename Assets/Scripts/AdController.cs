﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;

public class AdController : MonoBehaviour
{
    void Start()
    {
        if (Advertisement.isSupported)
            Advertisement.Initialize("4242741", false);
    }

    public void ShowAd()
    {
        if (Advertisement.IsReady())
            Advertisement.Show("Interstitial_Android");
    }
}
