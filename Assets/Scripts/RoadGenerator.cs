﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadGenerator : MonoBehaviour
{
    public GameObject[] Road;
    public bool[] roadNumbers;
    public int maxRoadLength = 0;
    public float distanceBtwRoads = 10;
    public float speedRoad = 5;
    public float maxPosY = -15;
    public GameObject startBg;
    private Vector3 waitingZone = new Vector3 (0, -40, 0);
    private List<GameObject> ReadyRoad = new List<GameObject>();
    private int currentRoadLength = 0;
    private int roadGenStatus = 1;
    private int currentRoadNumber = -1;
    private int lastRoadNumber = -1;
    private void FixedUpdate()
    {
        if (roadGenStatus == 1)
            if (currentRoadLength != maxRoadLength)
            {
                currentRoadNumber = Random.Range(0, Road.Length);
                if (currentRoadNumber != lastRoadNumber)
                {
                    if (currentRoadNumber < Road.Length / 2)
                        if (roadNumbers[currentRoadNumber] != true)
                            RoadCreate();

                    else if (currentRoadNumber >= Road.Length / 2)
                        if (roadNumbers[currentRoadNumber] != true)
                            RoadCreate();
                }
            }
        RoadMove();

        if (ReadyRoad.Count != 0)
            RoadRemove();
    }

    private void RoadMove()//Движение дороги
    {
        foreach (GameObject readyRoad in ReadyRoad)
            readyRoad.transform.localPosition -= new Vector3(0f, speedRoad * Time.fixedDeltaTime, 0f);
        if (startBg)
            startBg.transform.localPosition = new Vector3(0f, startBg.transform.localPosition.y - 0.136f, 0f);
    }

    private void RoadRemove()//Удаление дороги
    {
        if (ReadyRoad[0].transform.localPosition.y < maxPosY)
        {
            int i;
            i = ReadyRoad[0].GetComponent<Road>().number;
            roadNumbers[i] = false;
            ReadyRoad[0].transform.localPosition = waitingZone;
            ReadyRoad.RemoveAt(0);
            currentRoadLength--;
        }
    }

    private void RoadCreate()//Создание дороги
    {
        if (ReadyRoad.Count > 0)
            Road[currentRoadNumber].transform.localPosition = ReadyRoad[ReadyRoad.Count - 1].transform.position + new Vector3(0f, distanceBtwRoads, 0f);
        else if (ReadyRoad.Count == 0)
            Road[currentRoadNumber].transform.localPosition = new Vector3(0f, 8.75f, 0f);
        Road[currentRoadNumber].GetComponent<Road>().number = currentRoadNumber;
        roadNumbers[currentRoadNumber] = true;
        lastRoadNumber = currentRoadNumber;
        ReadyRoad.Add(Road[currentRoadNumber]);
        currentRoadLength++;
        CreatePlatform(Random.Range(0, 21));
    }

    private void CreatePlatform(int create)//Включение платформы (происходит с определённым шансом)
    {
        GameObject platform = Road[currentRoadNumber].transform.GetChild(2).gameObject;
        if (create > 18)
        {
            platform.SetActive(true);
            int j = Random.Range(0, 2);
            if (j == 0)
                platform.transform.localPosition = new Vector3(-0.69f, Random.Range(-0.9f, 0.9f), 0f);
            else
                platform.transform.localPosition = new Vector3(0.69f, Random.Range(-0.9f, 0.9f), 0f);
            if (startBg && startBg.transform.localPosition.y < -11)
                startBg.SetActive(false);
        }
    }

    public void restart()//Перезапуск игры
    {
        for (int num = 0; num < roadNumbers.Length; num++)
        {
            roadNumbers[num] = false;
        }

        foreach (GameObject road in Road)
        {
            road.transform.localPosition = new Vector3(0, -40, 0);
        }
        startBg.SetActive(true);
        startBg.transform.localPosition = new Vector3(0, 0.6f, 0);
    }
}