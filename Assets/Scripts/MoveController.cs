﻿using UnityEngine;

public class MoveController : MonoBehaviour
{
    public float sideSpeed = 1;
    public GameObject destroyFlame, destroySmoke;
    public Sprite[] shuttleSprites;
    public AudioClip[] audioClips;
    private int leftRightShift = 0;

    private void FixedUpdate()
    {
        if (GetComponent<SpriteRenderer>().sprite != shuttleSprites[2])
            transform.localPosition = new Vector3(transform.localPosition.x + sideSpeed * leftRightShift, -2f, 0f);
    }

    public void LeftButtonDown()
    {
        if (GetComponent<SpriteRenderer>().sprite != shuttleSprites[2])
        {
            if (transform.GetChild(4).GetComponent<AudioSource>().isActiveAndEnabled)
                transform.GetChild(4).GetComponent<AudioSource>().Play();
            leftRightShift = -1;
            GetComponent<SpriteRenderer>().sprite = shuttleSprites[1];
            GetComponent<SpriteRenderer>().flipX = true;
            GetComponent<CapsuleCollider2D>().size = new Vector2(1.5f, 3.3f);
            transform.GetChild(2).gameObject.transform.localPosition = new Vector3(-0.1f, -2.61f, 0);
            transform.GetChild(3).gameObject.SetActive(false);
        }
    }

    public void LeftRightButtonUp()
    {
        if (GetComponent<SpriteRenderer>().sprite != shuttleSprites[2])
        {
            leftRightShift = 0;
            GetComponent<SpriteRenderer>().sprite = shuttleSprites[0];
            GetComponent<SpriteRenderer>().flipX = false;
            GetComponent<CapsuleCollider2D>().size = new Vector2(2.15f, 3.3f);
            transform.GetChild(3).gameObject.SetActive(true);
            transform.GetChild(2).gameObject.transform.localPosition = new Vector3(0.38f, -2.61f, 0);
        }
    }
    
    public void RightButtonDown()
    {
        if (GetComponent<SpriteRenderer>().sprite != shuttleSprites[2])
        {
            leftRightShift = 1;
            GetComponent<SpriteRenderer>().sprite = shuttleSprites[1];
            GetComponent<SpriteRenderer>().flipX = false;
            GetComponent<CapsuleCollider2D>().size = new Vector2(1.5f, 3.3f);
            transform.GetChild(3).gameObject.SetActive(false);
            transform.GetChild(2).gameObject.transform.localPosition = new Vector3(0.1f, -2.61f, 0);
            if (transform.GetChild(4).GetComponent<AudioSource>().isActiveAndEnabled)
                transform.GetChild(4).GetComponent<AudioSource>().Play();
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "enemy" || collision.gameObject.tag == "platform")
        {
            GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.None;
            GetComponent<SpriteRenderer>().sprite = shuttleSprites[2];
            destroyFlame.SetActive(true);
            destroySmoke.SetActive(true);
            Time.timeScale = 0.8f;
            if (PlayerPrefs.GetString("Sound") == "on")
                {
                    transform.GetChild(4).GetComponent<AudioSource>().clip = audioClips[0];
                    transform.GetChild(4).GetComponent<AudioSource>().volume = 1f;
                    transform.GetChild(4).GetComponent<AudioSource>().Play();
                    transform.GetChild(5).GetComponent<AudioSource>().clip = audioClips[1];
                    transform.GetChild(5).GetComponent<AudioSource>().enabled = true;
                }
        }
    }
}